def sumar(x,y):
    resultado = x + y
    return resultado

def restar(x,y):
    resultado = x - y
    return resultado

def calculadora():
    suma = sumar(1,2)
    print(f'Suma de 1 y 2 = {suma}')
    suma = sumar(3,4)
    print(f'Suma de 3 y 4 = { suma}')
    resta = restar(5,6)
    print(f'Resta de 5 y 6 = {resta}')
    resta = restar(7,8)
    print(f'Resta de 7 y 8 = {resta}')

if __name__ == "__main__":
    calculadora()